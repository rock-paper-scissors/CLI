'use strict'

const App = require('./app')

const GameModeViewModel  = require('./view-models/game-mode')
const GameModeController = require('./controllers/game-mode')

const config = {
  startingScreen : {
    viewModel  : GameModeViewModel,
    controller : GameModeController,
  },
}

App.init(config).run()

