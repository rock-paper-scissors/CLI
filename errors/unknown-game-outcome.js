'use strict'

module.exports = class UnknownGameOutcomeError extends Error {

  constructor() {
    super('Unknown game outcome!')
    this.name = this.constructor.name
  }

}

