'use strict'

module.exports = class UnknownGameModeError extends Error {

  constructor() {
    super('Unknown game mode selected!')
    this.name = this.constructor.name
  }

}

