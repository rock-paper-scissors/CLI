'use strict'

const Presenter = require('./presenters/Presenter')
const Path      = require('./state/path')

const App = () => {

  let _config

  return {

    init(config) {
      _config = config
      return this
    },

    run() {
      Path.push(_config.startingScreen)
      Presenter.present(Path.last.screen.viewModel(null, Path.last.screen))
    },

  }

}

module.exports = App()

