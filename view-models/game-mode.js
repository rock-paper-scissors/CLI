'use strict'

const STRINGS = require('../res/strings')

const GameModeViewModel = (data, { controller }) => Object({
  title : {
    label  : STRINGS.MENUS.GAME_MODE.LABEL,
    marker : STRINGS.MENUS.GAME_MODE.MARKER,
  },
  menuItems : STRINGS.MENUS.GAME_MODE.ITEMS.map(menuItem => Object(
    {
      label   : menuItem.LABEL,
      marker  : menuItem.MARKER,
      handler : controller.selectionHandler,
    }
  )),
  submenuItems : {
    back : null,
    exit : {
      label : STRINGS.COMMON.EXIT,
    },
  }
})

module.exports = GameModeViewModel

