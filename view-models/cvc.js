'use strict'

const STRINGS = require('../res/strings')

const CvcViewModel = (data, { controller }) => Object({
  title : {
    label  : (data && data.label ) || STRINGS.MENUS.CVC.LABEL,
    marker : (data && data.marker) || STRINGS.MENUS.CVC.MARKER,
  },
  menuItems : STRINGS.MENUS.CVC.ITEMS.map(menuItem => Object(
    {
      label   : menuItem.LABEL,
      marker  : null,
      handler : controller.selectionHandler,
    }
  )),
  submenuItems : {
    back : {
      label   : STRINGS.COMMON.CHANGE_GAME_MODE,
      marker  : null,
      handler : controller.returnHandler,
    },
    exit : {
      label : STRINGS.COMMON.EXIT,
    },
  }
})

module.exports = CvcViewModel

