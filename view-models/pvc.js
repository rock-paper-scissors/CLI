'use strict'

const STRINGS = require('../res/strings')

const PvcViewModel = (data, { controller }) => Object({
  title : {
    label  : (data && data.label ) || STRINGS.MENUS.PVC.LABEL,
    marker : (data && data.marker) || STRINGS.MENUS.PVC.MARKER,
  },
  menuItems : STRINGS.MENUS.PVC.ITEMS.map(menuItem => Object(
    {
      label   : menuItem.LABEL,
      marker  : null,
      handler : controller.selectionHandler,
    }
  )),
  submenuItems : {
    back : {
      label   : STRINGS.COMMON.CHANGE_GAME_MODE,
      marker  : null,
      handler : controller.returnHandler,
    },
    exit : {
      label : STRINGS.COMMON.EXIT,
    },
  }
})

module.exports = PvcViewModel

