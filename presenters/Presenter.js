'use strict'

const createMenu = require('simple-terminal-menu')

// TODO: Move to global config
const INTERFACE_WIDTH = 58

const Presenter = {

  // TODO: Remove all logic from here
  present(viewModel) {

    const menu = createMenu({
      width : INTERFACE_WIDTH
    })

    menu.writeLine(
      viewModel.title.label,
      viewModel.title.marker,
    )

    menu.writeSeparator()
    viewModel.menuItems.forEach(menuItem => {
      menu.add(menuItem.label, menuItem.marker, menuItem.handler)
    })

    menu.writeSeparator()
    if (viewModel.submenuItems.back) {
      menu.add(
        viewModel.submenuItems.back.label,
        viewModel.submenuItems.back.marker,
        viewModel.submenuItems.back.handler,
      )
    }
    menu.add(viewModel.submenuItems.exit.label, menu.close)
  },

}

module.exports = Presenter

