'use strict'

const ApplicationPath = {

  screens : [],
  first   : null,
  last    : null,

  push(newScreen) {
    const screen = {
      "screen" : newScreen,
      "prev"   : null,
      "next"   : null,
    }
    if (this.last === null) {
      this.first = screen
      this.last  = screen
    }
    else {
      this.last.next = screen
      screen.prev    = this.screens[this.screens.length - 1]
      this.last      = screen
    }
    this.screens.push(screen)
  },

  pop() {
    this.last      = this.last.prev
    this.last.next = null
    this.screens.pop()
  },

}

module.exports = ApplicationPath

