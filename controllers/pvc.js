'use strict'

const Path                    = require('../state/path')
const BaseController          = require('./base')
const Presenter               = require('../presenters/Presenter')
const GameService             = require('../services/game')
const inMemoryStore           = require('../storage/in-memory-store')
const UnknownGameOutcomeError = require('../errors/unknown-game-outcome')

const GAME_OUTCOMES = {
  WIN  : 'Win',
  DRAW : 'Draw',
}

function updateStorage(winner) {
  if (!winner) {
    return
  }
  if (winner.name === 'Player') {
    inMemoryStore.wins++
  }
  else {
    inMemoryStore.losses++
  }
}

function constructGameOutcomeString(payload) {
  const [
    {
      name   : p1,
      weapon : w1,
    },
    {
      name   : p2,
      weapon : w2,
    },
  ] = payload.players
  const { outcome, winner } = payload

  const playerChoices = `${p1}: ${w1}, ${p2}: ${w2}`
  let gameOutcome = playerChoices

  switch (outcome) {
    case GAME_OUTCOMES.WIN:
      gameOutcome += ` - ${winner.name} wins!`
      break
    case GAME_OUTCOMES.DRAW:
      gameOutcome += ` - it's a draw!`
      break
    default:
      throw UnknownGameOutcomeError()
  }
  return gameOutcome
}

class PvcController extends BaseController {

  async selectionHandler(selection) {
    const response = await GameService.CreateNewPvcGame({ "weapon" : selection })
    const payload  = JSON.parse(response.payload)

    const gameOutcome = constructGameOutcomeString(payload)

    updateStorage(payload.winner)

    const { wins, losses } = inMemoryStore

    const view = Path.last.screen.viewModel({
      label  : gameOutcome,
      marker : `(W: ${wins}, L: ${losses})`,
    }, Path.last.screen)
    Presenter.present(view)
  }

}

module.exports = new PvcController()

