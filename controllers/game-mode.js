'use strict'

const Path                 = require('../state/path')
const BaseController       = require('./base')
const Presenter            = require('../presenters/Presenter')

const CvcGameController    = require('./cvc')
const CvcViewModel         = require('../view-models/cvc')

const PvcGameController    = require('./pvc')
const PvcViewModel         = require('../view-models/pvc')

const UnknownGameModeError = require('../errors/unknown-game-mode')

class GameModeController extends BaseController {

  selectionHandler(selection) {
    // TODO: Move to a factory.
    switch (selection) {
      case 'PvC':
        Path.push({
          controller : PvcGameController,
          viewModel  : PvcViewModel,
        })
        break
      case 'CvC':
        Path.push({
          controller : CvcGameController,
          viewModel  : CvcViewModel,
        })
        break
      default:
        throw new UnknownGameModeError()
    }
    const view = Path.last.screen.viewModel(null, Path.last.screen)
    Presenter.present(view)
  }

}

module.exports = new GameModeController()

