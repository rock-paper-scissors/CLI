'use strcit'

const Path         = require('../state/path')
const Presenter    = require('../presenters/Presenter')

module.exports = class BaseController {

  returnHandler() {
    Path.pop()
    const view = Path.last.screen.viewModel(null, Path.last.screen)
    Presenter.present(view)
  }

}

