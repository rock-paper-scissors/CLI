'use strict'

const Path            = require('../state/path')
const BaseController  = require('./base')
const Presenter       = require('../presenters/Presenter')
const GameService     = require('../services/game')

const UnknownGameOutcomeError = require('../errors/unknown-game-outcome')

const GAME_OUTCOMES = {
  WIN  : 'Win',
  DRAW : 'Draw',
}

class CvcController extends BaseController {

  async selectionHandler() {
    const response = await GameService.CreateNewCvcGame()
    const payload  = JSON.parse(response.payload)
    const [
      {
        name   : p1,
        weapon : w1,
      },
      {
        name   : p2,
        weapon : w2,
      },
    ] = payload.players
    const { outcome, winner } = payload

    const playerChoices = `${p1}: ${w1}, ${p2}: ${w2}`
    let gameOutcome = playerChoices

    switch (outcome) {
      case GAME_OUTCOMES.WIN:
        gameOutcome += ` - ${winner.name} wins!`
        break
      case GAME_OUTCOMES.DRAW:
        gameOutcome += ` - it's a draw!`
        break
      default:
        throw UnknownGameOutcomeError()
    }

    const view = Path.last.screen.viewModel({
      label : gameOutcome,
    }, Path.last.screen)
    Presenter.present(view)
  }

}

module.exports = new CvcController()

