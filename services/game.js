'use strict'

const Wreck = require('wreck')

// TODO: Move to env
const API_HOST = 'http://127.0.0.1:3000'

const GameService = {

  async CreateNewCvcGame() {
    try {
      return Wreck.post(`${API_HOST}/games/cvc`)
    }
    catch (ex) {
      console.error(ex)
    }
  },

  async CreateNewPvcGame(payload) {
    try {
      const options = {
        payload : JSON.stringify(payload),
        headers : {
          'Conten-Type' : 'application/json'
        },
      }
      return Wreck.post(`${API_HOST}/games/pvc`, options)
    }
    catch (ex) {
      console.error(ex)
    }
  }

}

module.exports = GameService

