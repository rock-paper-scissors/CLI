'use strict'

const inMemoryStore = {
  wins   : 0,
  losses : 0,
}

module.exports = inMemoryStore

